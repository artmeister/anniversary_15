const path = require('path');
const glob = require('glob');

const I18nPlugin = require('i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const LOCALES = glob.sync('./src/locales/*.json').map((file) => ({
  language: path.basename(file, path.extname(file)),
  translation: require(file),
}));

const available_locales = LOCALES.map(({ language, translation }) => ({
  code: language,
  name: translation[language],
}));

module.exports = LOCALES.map(({ language, translation }) => ({
  entry: {
    main: './src/index.js',
  },
  output: {
    filename: (chunkData) => (chunkData.chunk.name === 'main' ? '[name].[chunkhash].js' : 'banner/[name].js'),
    path: path.resolve(__dirname, `public/${language}`),
  },
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          'ejs-loader',
          'extract-loader',
          {
            loader: 'html-loader',
            options: {
              interpolate: true,
              removeComments: true,
              minimize: true,
              attrs: [":src"],
            },
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('precss'),
                require('autoprefixer'),
              ],
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'fonts',
            },
          },
        ],
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              outputPath: 'images',
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new I18nPlugin(translation, {
      functionName: 't',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      chunks: ['main'],
      template: './src/views/index.html',
      favicon: './src/images/favicon.ico',
      available_locales,
      locale: language,
    }),
  ],
}));
