const path = require('path');
const glob = require('glob');

const I18nPlugin = require('i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const LOCALES = glob.sync('./src/locales/*.json').map((file) => ({
  language: path.basename(file, path.extname(file)),
  translation: require(file),
}));

const available_locales = LOCALES.map(({ language, translation }) => ({
  code: language,
  name: translation[language],
}));

module.exports = LOCALES.map(({ language, translation }) => ({
  entry: './src/index.js',
  output: {
    filename: '[name].js',
    path: path.resolve(
      __dirname,
      language === 'ru' ? 'public' : `public/${language}`,
    ),
  },
  mode: 'development',
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    compress: true,
    port: 8001,
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          'ejs-loader',
          'extract-loader',
          {
            loader: 'html-loader',
            options: {
              interpolate: true,
              attrs: [":src"],
            },
          },
        ],
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/,
        use: 'file-loader',
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new I18nPlugin(translation, {
      functionName: 't',
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      chunks: ['main'],
      template: './src/views/index.html',
      favicon: './src/images/favicon.ico',
      available_locales,
      locale: language,
    }),
  ],
}));