const header = document.querySelector('.header');
const firstSection = document.querySelector('section');

const debounce = (func, wait) => {
	let timeout;

	return function (...args) {
		const context = this;

		clearTimeout(timeout);

		timeout = setTimeout(() => func.apply(context, args), wait);
	};
};

const onScroll = () => {
	let currScrollTop = document.documentElement.scrollTop;

	let isHeaderVisible = currScrollTop < header.offsetHeight;
	let isHeaderVisiblePlus = currScrollTop < header.offsetHeight * 2;
	let isHeaderOver = currScrollTop < firstSection.offsetHeight;

	header.classList.toggle('__sticky', !isHeaderVisible);
	header.classList.toggle(
		'__transition',
		!isHeaderVisible && isHeaderVisiblePlus
	);
	header.classList.toggle('__basic', !isHeaderOver);

	lastScrollTop = currScrollTop;
};

document.addEventListener('scroll', debounce(onScroll, 16));