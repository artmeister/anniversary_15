const navElement = document.getElementById("nav");
const toggleElement = document.getElementById("toggle");

const toggleHandler = (e) => {
  e.stopPropagation();

  navElement.classList.toggle("on");
  toggleElement.classList.toggle("on");
};

toggleElement.addEventListener("click", toggleHandler);