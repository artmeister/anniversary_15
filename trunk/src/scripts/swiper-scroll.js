const swiperContainers = document.querySelectorAll('.scrolled');

const windowScroll = swiperElem => {
  let isScrolled = localStorage.getItem(swiperElem.dataset.identify) || false;
  const bottomCoordSwiper = swiperElem.offsetTop + swiperElem.offsetHeight;

  window.addEventListener('scroll', () => {
    const windowPosition = window.scrollY + window.screen.availHeight;
    if (bottomCoordSwiper <= windowPosition && !isScrolled) {
      isScrolled = swiperScroll(swiperElem);
    }
  })
}

const swiperScroll = (swiperElem) => {
  document.body.classList.add('stop-scrolling');

  const lastSlide = swiperElem.querySelector('.last');
  swiperElem.addEventListener('mousewheel', () => {
    if (isSlideLast(swiperElem, lastSlide)) {
      document.body.classList.remove('stop-scrolling')
      localStorage.setItem(swiperElem.dataset.identify, true)
    }
  })
  return true
}

const isSlideLast = (swiperElem, lastSlide) => {
  return swiperElem.querySelector('.swiper-slide-active') === lastSlide;
}

swiperContainers.forEach(swiperElem => {
  // windowScroll(swiperElem);   
})