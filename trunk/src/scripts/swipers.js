import Swiper from 'swiper/bundle';

if (window.matchMedia("(min-width: 769px)").matches) {
	let swiperVertical = new Swiper('.swiper-container-vertical', {
		slidesPerView: 1,
		spaceBetween: 30,
		mousewheel: {
			releaseOnEdges: true,
		},
		loop: true,
		speed: 1000,
		grabCursor: true,
		direction: 'vertical',
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
			clickable: true,
		},
	});

} else if (window.matchMedia("(max-width: 768px)").matches) {
	let swiperVertical = new Swiper('.swiper-container-vertical', {
		init: false
	});
}