    const languageSwitcherElement = document.getElementById("js-language-switcher");
    const languageSwitcherListElement = document.getElementById("js-language-switcher-list");
    const languageSwitcherName = document.querySelector("#js-language-switcher .language-switcher__name");


    const languageSwitcherHandler = () => {
        languageSwitcherListElement.classList.toggle("on");
        languageSwitcherName.classList.toggle("on");
    }

    if (languageSwitcherListElement !== null) {
        languageSwitcherElement.addEventListener("click", languageSwitcherHandler);
    }