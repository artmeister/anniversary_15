import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

ScrollTrigger.matchMedia({
	'(max-width: 768px)': function () {
		const items = gsap.utils.toArray('.parallax');

		items.forEach((item, index) => {
			gsap.to(item, {
				y: 100,
				scrollTrigger: {
					trigger: `.parallax-${index}`,
					start: 'top center',
					end: 'center center',
					//markers: true,
					scrub: true,
					toggleActions: 'play reverse restart reverse',
					onToggle: (self) =>
						gsap.to(self.trigger, {
							opacity: self.isActive ? 1 : 0,
						}),
					onEnter: () => {
						const homeSection = document.querySelector(
							`.parallax-${index}`
						);
						homeSection.classList.add('active');
					},
				},
			});
		});

		gsap.to(".parallax-title", {
			scrollTrigger: {
				trigger: ".parallax-title",
				start: "center center",
				endTrigger: ".parallax-2",
				end: "center center",
				scrub: true,
				//markers: true,
				pin: true,
				pinSpacing: false,
				toggleActions: 'play reverse restart reverse',
				onToggle: (self) =>
					gsap.to(self.trigger, {
						opacity: self.isActive ? 1 : 0,
				}),
				onEnter: () => {
					const title = document.querySelector(
						".parallax-title"
					);
					title.classList.add('fixed');
				},
			},
		});
	},
});
