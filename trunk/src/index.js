import './styles/index.scss';
import './scripts/swipers';
import './scripts/swiper-scroll';
import './scripts/mobile-menu';
import './scripts/parallax';
import './scripts/lang';